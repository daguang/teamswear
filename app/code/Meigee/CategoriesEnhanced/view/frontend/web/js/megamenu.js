require(['jquery'], function ($) {
	jQuery(document).ready(function(){
		function tabsMenu() {
			isRtl = false;
			if(jQuery('body').hasClass('rtl')){
				isRtl = true;
			}
			if (jQuery(document.body).width() > 1007){
				jQuery('.megamenu li.level-top .megamenu-wrapper.tabs-menu').each(function() {
					var columnsCount = jQuery(this).data('columns');
					var isVerticalTabs = jQuery(this).hasClass('vertical');

					jQuery(this).find('.megamenu-inner').each(function() {
						var submenuColumnsCount = jQuery(this).data('columns');
						jQuery(this).find('li.level2').each(function() {
							jQuery(this).css({'width' : 100/submenuColumnsCount + '%'});
						});
					});
				});
				setTimeout(function() {
					var windowHeight = jQuery(window).height();
					jQuery('.page-header .megamenu li.level-top .megamenu-wrapper.tabs-menu').each(function() {
						var maxHeight = 0;
						jQuery(this).find('.megamenu-inner').each(function() {
							if (jQuery(this).outerHeight() > maxHeight) {
								maxHeight = jQuery(this).outerHeight();
							}
						});
						jQuery(this).find('.megamenu-wrapper').each(function() {
							if (jQuery(this).outerHeight() > maxHeight) {
								maxHeight = jQuery(this).outerHeight();
							}
						});

						if (jQuery(this).hasClass('vertical')) {
							var setHeight = maxHeight + parseInt(jQuery(this).css('padding-top')) + parseInt(jQuery(this).css('padding-bottom'));
							jQuery(this).css('min-height', setHeight);
						} else {
							var setHeight = maxHeight + parseInt(jQuery(this).css('padding-top'))
												+ parseInt(jQuery(this).css('padding-bottom'))
												+ parseInt(jQuery(this).css('border-top-width'))
												+ parseInt(jQuery(this).css('border-bottom-width'))
												+ parseInt(jQuery(this).find('li.level1').height());
							if (setHeight+jQuery(this).offset().top > windowHeight) {
								setHeight = windowHeight - jQuery(this).offset().top - 40;
							}
							jQuery(this).css('min-height', setHeight);
						}
						jQuery(this).find('.megamenu-inner').each(function() {
							jQuery(this).css({'height' : maxHeight});
						});
						jQuery(this).find('.megamenu-wrapper').each(function() {
							jQuery(this).css({'height' : maxHeight});
						});
					});
				}, 1000);
			}
		}
		tabsMenu();
		function megamenuPos() {
	    	if (jQuery(document.body).width() > 1007){
				jQuery('.megamenu li.parent').each(function() {
					if (jQuery(this).find('.megamenu-wrapper').length > 0) {
						if (!jQuery('.header-wrapper').hasClass('vertical-header')) {
							jQuery(this).height();
				            jQuery(this).find('.megamenu-wrapper').css({
				                top: jQuery(this).position().top + jQuery(this).height()
				            })
						} else {
				            jQuery(this).find('.megamenu-wrapper').css({
				                top: jQuery(this).position().top,
				                width: jQuery('#maincontent .main-container > .content-inner').outerWidth() + 20
				            })				
						}
				    }
				});		
			}
		}	
		
		function mobileMenuIcon() {
			jQuery(".nav.topmenu li.parent").each(function() {
				if (!jQuery(this).find("> a.level-top .ui-menu-icon").length) {
					jQuery(this).find("> a.level-top").prepend('<span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>');
				}
				if(jQuery(this).children('ul.default-menu').length) {
					jQuery(this).addClass('default-menu-parent');
				}
			});	
		}
		function mobileCollapse() {
			if(jQuery(document.body).width() < 1008){
				jQuery('.nav.topmenu li.parent > a').each(function(){
					if(jQuery(this).siblings('ul').length || jQuery(this).siblings('div.megamenu-wrapper').length){
						jQuery(this).next('ul').slideUp('fast');
						jQuery(this).find('.ui-menu-icon').off().on('click', function(event){
							event.preventDefault();
							jQuery(this).closest('a').nextAll('ul, div.megamenu-wrapper').slideToggle('medium');
						});
					}
				});
			}
		}
		mobileMenuIcon();
        mobileCollapse();
		megamenuPos();
	    jQuery(window).on("scroll", function() {
	    	if (jQuery('#sticky-header[style*="display: block"]').length > 0) {
				megamenuPos();
	    	}
	    })
		jQuery(window).resize(function(){
			tabsMenu();
			megamenuPos();
			mobileMenuIcon();
			mobileCollapse();
		});
	
		jQuery('.megamenu li.level-top').on('mouseenter', function(event){
			jQuery(this).children('a.level-top').addClass('ui-state-focus');
			jQuery(this).addClass('menu-active');
		});
	
		jQuery('.megamenu li.level-top').on('mouseleave', function(event){
			jQuery(this).children('a.level-top').removeClass('ui-state-focus');
			jQuery(this).removeClass('menu-active');
		});
	
		jQuery('.megamenu li a.level-top').on('click', function(event){
			jQuery(this).toggleClass('ui-state-focus');
			jQuery(this).closest('li.level-top').toggleClass('menu-active');
		});
		
		columnsWidth = function(o, n) {
			if (n.size() > 1) {
				n.each(function() {
					jQuery(this).css("width", (100 / n.size()) + "%")
				})
			} else {
				n.css("width", (100 / o) + "%")
			}
		};
		jQuery(".megamenu .megamenu-wrapper").each(function() {
			columnsCount = jQuery(this).data("columns");
			items = jQuery(this).find("ul.level0 > li");
			groupsCount = items.size() / columnsCount;
			ratio = 1;
			for (i = 0; i < groupsCount; i++) {
				currentGroupe = items.slice((i * columnsCount), (columnsCount * ratio));
				columnsWidth(columnsCount, currentGroupe);
				ratio++
			}
		});
		
		
	});
	
});