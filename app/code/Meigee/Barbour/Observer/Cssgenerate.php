<?php
namespace Meigee\Barbour\Observer;
 
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
// use \Magento\Store\Model\ScopeInterface;

class Cssgenerate implements ObserverInterface
{
 
	 /**
     * @var RequestInterface
     */
    protected $appRequestInterface;
	private $_cssGenerate;

    public function __construct(
		\Magento\Backend\Block\Template\Context $context,
        RequestInterface $appRequestInterface,
		\Meigee\Barbour\Block\Frontend\CustomDesign $cssGenerate
    ) {
        $this->appRequestInterface = $appRequestInterface;
		$this->_cssGenerate = $cssGenerate;
    }
 
	public function execute(Observer $observer)
	{
		$sectionId = $this->appRequestInterface->getParam('section');
		$is_design = strrpos($sectionId, 'barbour_theme_design');
		
		// if($sectionId == 'barbour_theme_design' 
			// || $sectionId == 'barbour_theme_design_blue'
			// || $sectionId == 'barbour_theme_design_red'
			// || $sectionId == 'barbour_theme_design_green'
			// || $sectionId == 'barbour_theme_design_navi'
			// || $sectionId == 'barbour_theme_design_cloth'
			// || $sectionId == 'barbour_theme_design_cloth_orange'
			// || $sectionId == 'barbour_theme_design_cloth_yellow'
			// || $sectionId == 'barbour_theme_design_cloth_purple'
			// || $sectionId == 'barbour_theme_design_electronics1'
			// || $sectionId == 'barbour_theme_design_electronics1_red'
			// || $sectionId == 'barbour_theme_design_electronics1_green'
			// || $sectionId == 'barbour_theme_design_electronics1_black'
			// || $sectionId == 'barbour_theme_design_electronics2'
			// || $sectionId == 'barbour_theme_design_electronics2_red'
			// || $sectionId == 'barbour_theme_design_electronics2_blue'
			// || $sectionId == 'barbour_theme_design_electronics2_red2'
			// || $sectionId == 'barbour_theme_design_jewelry'
			// || $sectionId == 'barbour_theme_design_jewelry_beige'
			// || $sectionId == 'barbour_theme_design_jewelry_black'
			// || $sectionId == 'barbour_theme_design_jewelry_red'
			// || $sectionId == 'barbour_theme_design_perfume'
			// || $sectionId == 'barbour_theme_design_perfume_pink'
			// || $sectionId == 'barbour_theme_design_perfume_purple'
			// || $sectionId == 'barbour_theme_design_perfume_beige'
			// || $sectionId == 'barbour_theme_design_handmade'
			// || $sectionId == 'barbour_theme_design_handmade_blue'
			// || $sectionId == 'barbour_theme_design_handmade_pink'
			// || $sectionId == 'barbour_theme_design_handmade_beige') {
		if($is_design !== false) {
			 $this->_cssGenerate->saveOpt(true, $sectionId);
		}
	}
}

