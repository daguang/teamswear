#Install
####composer install

#Magento Authentication
####Public Key: 38bab543ce06c2a0022050d61bcb2031
####Private Key: 6b176820b9fe3c7ab203cc9e275fcc95

#Compile
####rm -rf var/page_cache
####rm -rf var/composer_home
####rm -rf var/generation
####rm -rf var/di
####rm -rf var/view_preprocessed
####php bin/magento setup:upgrade --keep-generated
####php bin/magento indexer:reindex
####php bin/magento setup:di:compile
####php bin/magento setup:static-content:deploy -f

#Rebuild
####php bin/magento setup:upgrade
####php bin/magento cache:flush
####php bin/magento setup:di:compile
####php bin/magento setup:static-content:deploy -f

#Admin user
####username: admin
####password: P@ssw0rd

#Composer run trouble
####sudo chmod -R 777 /home/hermes/.composer/

#Showing page trouble
####chmod -R 777 var/
####chmod -R 777 pub/
